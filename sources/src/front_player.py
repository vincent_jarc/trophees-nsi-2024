from tkinter import *
from back_players import *
from tkinter.ttk import *

class Interface_Player(Frame):
    def __init__(self, master, player):
        """
        Initialise l'instance de la classe Interface_Player.

        Args:
            master (Tk): Le widget parent dans lequel cette interface sera intégrée.
        """
        Frame.__init__(self, master)
        self.master = master
        self.player = player
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()

    def creer_widgets(self):
        """
        Crée les différents widgets de l'interface.
        """
        self.img_leader = Label(master=self)
        self.img_flag = Label(master=self)
        self.lbl_pays = Label(master=self)
        self.lbl_or = Label(master=self, text='or')
        self.lbl_population = Label(master=self, text='population')
        self.lbl_nouriture = Label(master=self, text='nouriture')
        self.lbl_reputation = Label(master=self, text='reputation')
        self.lbl_technologie = Label(master=self, text='technologie')
        self.lbl_villes = Label(master=self, text="villes")
        self.val_or = Label(master=self)
        self.val_population = Label(master=self)
        self.val_nouriture = Label(master=self)
        self.val_reputation = Label(master=self)
        self.val_technologie = Label(master=self)
        self.val_villes = Label(master=self)

    def configurer_widgets(self):
        """
        Configure les propriétés visuelles des widgets en fonction des données du joueur.
        """
        # self.master.config(bg=self.player.color)
        self.img_leader.config(image=self.images[str(self.player.id)]["leader"])
        self.img_flag.config(image=self.images[str(self.player.id)]["flag"])
        self.lbl_pays.config(text=str(self.player.name), font=(20), foreground=self.player.color)
        self.lbl_or.config(foreground=self.player.color)
        self.lbl_population.config(foreground=self.player.color)
        self.lbl_nouriture.config(foreground=self.player.color)
        self.lbl_reputation.config(foreground=self.player.color)
        self.lbl_technologie.config(foreground=self.player.color)
        self.lbl_villes.config(foreground=self.player.color)
        self.val_or.config(text=str(self.player.gold), foreground=self.player.color)
        self.val_population.config(text=str(self.player.population), foreground=self.player.color)
        self.val_nouriture.config(text=str(self.player.food), foreground=self.player.color)
        self.val_reputation.config(text=str(self.player.reputation), foreground=self.player.color)
        self.val_technologie.config(text=str(self.player.technologie), foreground=self.player.color)
        self.val_villes.config(text=str(self.player.cities), foreground=self.player.color)

    def charger_images(self):
        """
        Charge les différentes images nécessaires à l'interface.
        """
        data = charger_fichier_json("NATIONS")
        self.images = {}
        for nation in data:
            self.images[str(nation)] = {}
            self.images[str(nation)]["flag"] = PhotoImage(file="../pics/nations/" + str(data[nation]["flag_picture"]))
            self.images[str(nation)]["leader"] = PhotoImage(file="../pics/nations/" + str(data[nation]["leader_picture"]))

    def placer_widgets(self):
        """
        Place les widgets créés dans la fenêtre tkinter.
        """
        self.img_leader.grid(column=0, row=5, columnspan=1, rowspan=10)
        self.img_flag.grid(column=0, row=0, columnspan=1, rowspan=5)
        self.lbl_pays.grid(column=2, row=0, columnspan=1, rowspan=3)
        self.lbl_or.grid(column=2, row=3, columnspan=1, rowspan=1)
        self.lbl_population.grid(column=2, row=5, columnspan=1, rowspan=1)
        self.lbl_nouriture.grid(column=2, row=7, columnspan=1, rowspan=1)
        self.lbl_reputation.grid(column=2, row=9, columnspan=1, rowspan=1)
        self.lbl_technologie.grid(column=2, row=11, columnspan=1, rowspan=1)
        self.lbl_villes.grid(column=2, row=13, columnspan=1, rowspan=1)
        self.val_or.grid(column=2, row=4, columnspan=1, rowspan=1)
        self.val_population.grid(column=2, row=6, columnspan=1, rowspan=1)
        self.val_nouriture.grid(column=2, row=8, columnspan=1, rowspan=1)
        self.val_reputation.grid(column=2, row=10, columnspan=1, rowspan=1)
        self.val_technologie.grid(column=2, row=12, columnspan=1, rowspan=1)
        self.val_villes.grid(column=2, row=14, columnspan=1, rowspan=1)

    def actualiser(self, player=None):
        """
        Met à jour l'interface avec les données d'un joueur spécifique.

        Args:
            player (dict): Un dictionnaire représentant les attributs du joueur. Si None, aucun changement n'est effectué.
        """
        if player:
            self.player = player
            self.configurer_widgets()

if __name__ == "__main__":
    player=Player("NATION_USA")
    fenetre = Tk()
    ecran = Interface_Player(fenetre, player)
    ecran.actualiser(player)
    ecran.grid(row=0, column=2, columnspan=2)
    fenetre.mainloop()