from back_building import Building
from back_unit import Unit
from outils import *

class City:
    def __init__(self, id:str, city_datas:dict):
        self.id = id
        #self.data = city_datas["cities"][id]
        self.localisation = city_datas["cities"][id]["localisation"]
        self.name = city_datas["cities"][id]["name"]
        self.owner_id = city_datas["cities"][id]["owner"]
        self.hp = 100
        self.in_progress = None
        self.identifier  = None
        self.neighbors = []
        self.population = 1
        self.resources = {"food": 1, "gold": 1}
        self.unbuilt_buildings = []
        self.completed_buildings = []
        self.initialiser_batiments()
        self.units = []

    def __str__(self):
        return """   {0}
                POPULATION : {1}
                COORD ️    : {2}
                VOISINS    : {3}
                NOURRITURE : {4}
                OR         : {5}
                CONSTRUIT  : {6}
                """.format(self.id, self.population, self.localisation, self.neighbors, self.resources["food"], self.resources["gold"], self.completed_buildings)

    def __repr__(self):
        return "{0} ({1} hab)".format(self.name, self.population)

    def proprietaire(self) -> str:
        return self.owner_id

    def voisins(self) -> list:
        return self.neighbors

    def recolte(self) -> dict:
        """Renvoie un dictionnaire contenant les ressources de la ville
        et celle de tous ses bâtiments achevés"""
        total_ress = {"food":self.resources["food"],
                      "gold":self.resources["gold"]}
        for building in self.batiments_acheves():
            build_ress = building.recolte()
            total_ress["food"] += build_ress["food"]
            total_ress["gold"] += build_ress["gold"]
        return total_ress

    def batiments_acheves(self) -> list:
        return self.completed_buildings

    def batiments_endommages(self) -> list:
        return [build for build in self.completed_buildings if build.est_endommage()]

    def batiments_non_construits(self) -> list:
        return self.unbuilt_buildings

    def initialiser_batiments(self):
        """Crée tous les bâtiments possibles sans les construire"""
        data = charger_fichier_json("BUILDINGS")
        for building_id in data:
            self.unbuilt_buildings.append(Building(building_id, data))

    def en_attente(self) -> bool:
        """Indique si la ville attend un ordre de construction"""
        if self.in_progress is None:
            return True
        else:
            return False

    def contient_unites(self) -> bool:
        """Indique si la ville contient des unités"""
        if self.units:
            return True
        return False

    def unites_stationnees(self) -> list:
        """Renvoie la liste des unités stationnées"""
        return self.units

    def peut_ajouter_unite(self) -> bool:
        """DEPRECATED"""
        pass

    def unites_entrainables(self) -> list:
        """OPTIONNEL"""
        pass

    def calcule_defense(self) -> int:
        """OPTIONNEL"""
        pass

    def calcule_degats(self, other) -> int:
        pass

    def a_faim(self) -> bool:
        pass

    def change_proprietaire(self, owner_id):
        self.owner_id = owner_id

    def se_connecte_a(self, other):
        """Connecte entre elles les villes self et other"""
        self.neighbors.append(other)
        other.neighbors.append(self)

    def obtient_identifiant(self, id):
        self.identifier = id

    def commence_tour(self):
        self.ajoute_population()
        self.construit()

    def enleve_population(self, value=1):
        self.population -= value
        if self.population <= 0:
            self.population = 1

    def ajoute_population(self, value=1):
        self.population += value

    def ajoute_ressource(self, value, res_id):
        if res_id in self.resources:
            self.resources[res_id] += value
        else:
            self.resources[res_id] = value

    def enleve_ressource(self, value, res_id):
        if res_id in self.resources:
            self.resources[res_id] -= value
            if self.resources[res_id] < 0:
                self.resources[res_id] = 0

    def ajoute_unite(self, unit):
        self.units.append(unit)

    def enleve_unite(self, unit):
        self.units.remove(unit)

    def met_en_chantier(self, bat_id):
        for build in self.unbuilt_buildings:
            if build.id == bat_id:
                self.in_progress = build
                break

    def construit(self, value=1):
        self.in_progress.construit(value)
        if self.in_progress.est_acheve():
            self.unbuilt_buildings.remove(self.in_progress)
            self.completed_buildings.append(self.in_progress)
            self.in_progress = None

    def attaque_unite(self, other):
        """OPTIONNEL"""
        pass

    def enleve_hp(self, value):
        self.hp = max(0, self.hp - value)

    def ajoute_hp(self, value):
        self.hp = min(100, self.hp + value)


if __name__ == '__main__':
    c1 = City("CITY_Washington_D_C", charger_carte("MAP_WORLD.json"))
    c2 = City("CITY_Paris", charger_carte("MAP_WORLD.json"))
    c1.se_connecte_a(c2)
    print(c1)
    print([c1, c2])

    print("--------------")
    print("Construction d'une caserne : ")
    c1.met_en_chantier("BUILD_caserne")
    c1.construit(10)
    c1.ajoute_population(2)
    print(c1)

    print("--------------")
    print("Ajout d'une infanterie : ")
    u = Unit("UNIT_soldier")
    c1.ajoute_unite(u)
    print(c1.units)

    print("--------------")
    print("Retrait d'une infanterie : ")
    c1.enleve_unite(u)
    print(c1.units)