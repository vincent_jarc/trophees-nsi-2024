from tkinter import *
from back_city import City
from front_building import Interface_Construction


class Interface_Ville(Frame):
    def __init__(self, master):
        Frame.__init__(self, master,width=500, height=master.winfo_screenheight()//5)
        self.master = master
        self.ville_selectionnee = None
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self._isShow = False
        self.placer_widgets()

    def creer_widgets(self):
        """AJOUTER DOCUMENTATION ICI"""
        self.lbl_nom_ville = Label(master = self, text="Aucune ville sélectionnée")
        self.lbl_nom_owner = Label(master=self, text="Nation : -")
        self.lbl_units_ville = Label(master = self, text="Unités : -",image=self.icon_units, compound=LEFT)
        self.lbl_population_ville = Label(master = self, text="Population : -",image=self.icon_pop, compound=LEFT)
        self.lbl_food_ville = Label(master = self, text="Nourriture : -",image=self.icon_food, compound=LEFT)
        self.lbl_gold_ville = Label(master = self, text="Or : -", image=self.icon_gold, compound=LEFT)
        self.lbl_unbuild = Label(master = self, text="EEE")
        self.btn_unbuild = Button(self, image=self.unbuid_img,command=lambda: self.unbuild_view())
        self.btn_units = Button(self, image=self.units_img, command=lambda: self.units_view())

    def charger_images(self):
        """AJOUTER DOCUMENTATION ICI"""
        self.flag_img = PhotoImage(master=self)
        self.unbuid_img = PhotoImage(master=self,file="../pics/window/ICON_building.png")
        self.units_img = PhotoImage(master=self, file="../pics/window/ICON_training.png")
        self.icon_gold = PhotoImage(master=self, file="../pics/ressources/ICON_or.png").subsample(2, 2)
        self.icon_food = PhotoImage(master=self, file="../pics/ressources/ICON_nourriture.png").subsample(2, 2)
        self.icon_pop = PhotoImage(master=self, file="../pics/ressources/ICON_population.png").subsample(2, 2)
        self.icon_units = PhotoImage(master=self, file="../pics/ressources/ICON_technologie.png").subsample(2, 2)

    def placer_widgets(self):
        """AJOUTER DOCUMENTATION ICI"""
        self.lbl_nom_ville.grid(row=0, column=0, columnspan=2)
        self.lbl_nom_owner.grid(row=1, column=0, columnspan=2)
        self.lbl_population_ville.grid(row=2, column=0, sticky=W)
        self.lbl_units_ville.grid(row=2, column=1, sticky=W)
        self.lbl_food_ville.grid(row=3, column=0, sticky=W)
        self.lbl_gold_ville.grid(row=3, column=1, sticky=W)
        self.btn_unbuild.grid(row=4, column=0)
        self.btn_units.grid(row=4, column=1)

    def cacher_widgets(self):
        """AJOUTER DOCUMENTATION ICI"""
        self.lbl_nom_ville.grid_remove()
        self.lbl_nom_owner.grid_remove()
        self.lbl_population_ville.grid_remove()
        self.lbl_units_ville.grid_remove()
        self.lbl_food_ville.grid_remove()
        self.lbl_gold_ville.grid_remove()
        self.btn_unbuild.grid_remove()
        self.btn_units.grid_remove()

    def configurer_widgets(self):
        """AJOUTER DOCUMENTATION ICI"""
        self.lbl_nom_ville.config(font=("Arial 16"))
        self.lbl_nom_owner.config(font=("Arial 14"))
        self.lbl_population_ville.config(font=("Arial 10"), justify=LEFT)
        self.lbl_units_ville.config(font=("Arial 10"), justify=LEFT)
        self.lbl_food_ville.config(font=("Arial 10"), justify=LEFT)
        self.lbl_gold_ville.config(font=("Arial 10"), justify=LEFT)

    def actualiser(self, city, hidden=False):
        """AJOUTER DOCUMENTATION ICI"""
        if self._isShow or self.ville_selectionnee != city:
            self.placer_widgets()
            self.ville_selectionnee = city
            self.lbl_nom_ville.config(text=f"City {self.ville_selectionnee.name}")
            self.lbl_nom_owner.config(text=f"Nation : {self.ville_selectionnee.owner_id}")
            self.lbl_population_ville.config(text=f"Population : {self.ville_selectionnee.population}")
            if not hidden:
                self.lbl_units_ville.config(text=f"Unités - {self.ville_selectionnee.units}")
                self.lbl_food_ville.config(text=f"Nourriture : {self.ville_selectionnee.resources['food']}")
                self.lbl_gold_ville.config(text=f"Or : {self.ville_selectionnee.resources['gold']}")
            else:
                self.lbl_units_ville.grid_remove()
                self.lbl_food_ville.grid_remove()
                self.lbl_gold_ville.grid_remove()
                self.btn_unbuild.grid_remove()
                self.btn_units.grid_remove()
            self._isShow = False
        else:
            self.cacher_widgets()
            self._isShow = True

    def unbuild_view(self):
        """AJOUTER DOCUMENTATION ICI"""
        Interface_Construction(self.master, self.ville_selectionnee)

    def units_view(self):
        """AJOUTER DOCUMENTATION ICI"""
        print("Units")



if __name__ == "__main__":
    from outils import charger_carte
    c1 = City("CITY_Washington_D_C", charger_carte("../maps/MAP_WORLD.json"))
    c2 = City("CITY_Paris", charger_carte("../maps/MAP_WORLD.json"))

    fenetre = Tk()
    ecran = Interface_Ville(fenetre)
    btn1 = Button(fenetre,
                  text="Ville amie",
                  command=lambda: ecran.actualiser(c1))
    btn2 = Button(fenetre,
                  text="Ville ennemie",
                  command=lambda: ecran.actualiser(c2, hidden=True))
    ecran.grid(row=0, column=0, columnspan=2)
    btn1.grid(row=1, column=0)
    btn2.grid(row=1, column=1)
    fenetre.mainloop()
