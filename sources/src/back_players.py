from back_unit import Unit
from back_city import City
from outils import *


class Player:
    def __init__(self, nation_id: str):
        datas = charger_fichier_json("NATIONS")[nation_id]
        self.id = nation_id
        self.name = datas["name"]
        self.color = datas["color"]
        self.leader = datas["leader"]
        self.leader_picture = "../pics/nations/" + datas["leader_picture"]
        self.flag_picture = "../pics/nations/" + datas["flag_picture"]
        self.bonus = datas["bonus"]
        self.gold = 100
        # TODO : la nourriture ne sert-elle pas à augmenter la population des villes ?
        self.food = 10
        self.cities = []
        self.units = []
        self.population = 10
        self.reputation = 0
        self.technologie = 0
        

    def __str__(self):
        txt = "\t" * 2 + self.name.upper() + "\n"
        txt += "LEADER : " + self.leader + "\n"
        txt += "BONUS  : " + self.bonus + "\n"
        txt += "VILLES : \n"
        for city in self.cities:
            txt += "\t - " + city.name + "\n"
        txt += "OR :" + str(self.gold) + "\n"
        txt += "NOURRITURE :" + str(self.food)
        return txt

    def __repr__(self):
        return self.name

    def est_mort(self) -> bool:
        """Indique si le joueur a fini de jouer ou non"""
        return self.cities == []

    def a_fini_tour(self) -> bool:
        """On s'assure que toutes les unités ont fini d'agir et que toutes les villes ont du travail"""
        if self.a_ville_en_attente() or self.a_unite_en_attente():
            return False
        else:
            return True

    def villes_conquises(self) -> list:
        """renvoie la liste des villes du joueur"""
        return self.cities

    def a_ville_en_attente(self) -> bool:
        """Indique si le joueur a une ville en attente de construction"""
        for city in self.cities:
            if city.en_attente():
                return True
        return False

    def possede_unite(self, tag: str) -> bool:
        """Indique si le joueur détient l'unité portant l'étiquette tag"""
        for unit in self.units:
            if unit.tag == tag:
                return True
        return False

    def cherche_unite(self, tag: str) -> Unit:
        """Renvoie l'unité portant l'étiquette tag, None sinon"""
        for unit in self.units:
            if unit.tag == tag:
                return unit
        return None

    def a_unite_en_attente(self) -> bool:
        """Indique si le joueur a une unité en attente d'ordre"""
        for unit in self.units:
            if unit.peut_agir():
                return True
        return False

    def commence_tour(self):
        """applique les différentes actions de début de tour aux villes et unités du joueur"""
        for unit in self.units:
            unit.commence_tour()
        for city in self.cities:
            city.commence_tour()
        self.recolte()

    def ajoute_unite(self, unit: Unit):
        """ajoute l'unité indiquée de la liste des unités du joueur"""
        self.units.append(unit)

    def enleve_unite(self, unit: Unit):
        """enlève l'unité indiquée de la liste des unités du joueur"""
        self.units.remove(unit)

    def ajoute_ressources(self, value: int, res_id: str):
        """ajoute au joueur les ressources contenues dans le dictionnaire"""
        if res_id == "food":
            self.food += value
        elif res_id == "gold":
            self.gold += value

    def enleve_ressources(self, value: int, res_id: dict):
        """enlève au joueur les ressources contenues dans le dictionnaire"""
        if res_id == "food":
            self.food = max(0, self.food - value)
        elif res_id == "gold":
            self.gold = max(0, self.gold - value)

    def recolte(self):
        """récolte les ressources produites par les villes et bâtiments afin
        de les stocker dans la réserve du joueur"""
        for city in self.cities:
            recolte = city.recolte()
            x = recolte["gold"]
            y = recolte["food"]
            self.ajoute_ressources(x, "gold")
            self.ajoute_ressources(y, "food")

    def ajoute_ville(self, city: City):
        """ajoute la ville indiquée de la liste des villes détenues par le joueur"""
        self.cities.append(city)

    def enleve_ville(self, city: City):
        """enlève la ville indiquée de la liste des villes détenues par le joueur"""
        self.cities.remove(city)


if __name__ == '__main__':
    j1 = Player("NATION_USA")
    j2 = Player("NATION_GREENLAND")
    c1 = City("CITY_Washington_D_C", charger_carte("MAP_WORLD.json"))
    c2 = City("CITY_Paris", charger_carte("MAP_WORLD.json"))
    j1.ajoute_ville(c1)
    j1.ajoute_ville(c2)
    print(j1, end="\n------\n")
    print([j1, j2], end="\n------\n")
    print("Joueur 1 a terminé ?", j1.a_fini_tour(), end="\n------\n")
    j1.recolte()
    print("Récolte du joueur 1 !!!\n", j1, end="\n------\n")
