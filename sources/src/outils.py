from random import randrange
import json
#from vlc import MediaPlayer, Instance, PlaybackMode
"""

# fonction OK
def jouer_bruitage(sound_id:str):
    lecteur_bruit = MediaPlayer("../sound/SOUND_" + sound_id + ".mp3")
    lecteur_bruit.play()

# fonction OK
def jouer_musique_ambiance():
    instance = Instance()
    liste = instance.media_list_new()
    lecteur = instance.media_list_player_new()
    for i in range(1, 7):
        x=randrange(1,7)
        noms_morceaux = ["../sound/MUSIC_atmosphere"+str(x)+".mp3"]
    for morceau in noms_morceaux:
        liste.add_media(instance.media_new(morceau))

    lecteur.set_media_list(liste)
    lecteur.play()
    lecteur.set_playback_mode(PlaybackMode.loop)

#probleme au niveau des musiques
def jouer_musique(sound_id:str):
    instance = Instance()
    liste = instance.media_list_new()
    lecteur = instance.media_list_player_new()

    noms_morceaux = [hasard_musique(sound_id),hasard_musique(sound_id),hasard_musique(sound_id)]
    for morceau in noms_morceaux:
        liste.add_media(instance.media_new(morceau))

    lecteur.set_media_list(liste)
    lecteur.set_playback_mode(PlaybackMode.loop)
    lecteur.play()
"""

def charger_fichier_json(filetype:str)->dict:
    """Charge les fichiers de données du jeu. Le paramètre filetype peut valoir :
    BUILDINGS, MUSIC, NATIONS, RESOURCES, SOUNDS ou UNITS"""
    with open("../datas/DATA_{}.json".format(filetype),"r") as f:
        return json.load(f)

def charger_carte(mapfile:str)->dict:
    """Charge un fichier carte. Le paramètre mapfile correspond au nom du fichier"""
    with open(mapfile, "r") as f:
        return json.load(f)



if __name__ == '__main__':
    print(charger_fichier_json("RESOURCES"))
    print(charger_carte("MAP_ASIA.json"))
    jouer_bruitage("blast")
    jouer_musique_ambiance() #pas d'arret

   
