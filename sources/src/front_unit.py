from tkinter import *
from back_unit import *
from outils import *
from tkinter.ttk import *


class Interface_Unite(Frame):
    def __init__(self, master):
        """
        Constructeur initial de l'interface.
        """
        Frame.__init__(self, master)
        self.master = master
        self.unite_selectionnee = None
        self.charger_images()

        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()

    def creer_widgets(self):
        """
        Crée les différents boutons et labels de l'interface de base.
        """
        self.img_unit = Label(master=self)

        self.btn_attaque = Button(master=self)
        self.btn_repos = Button(master=self)
        self.btn_deplacer = Button(master=self)

        self.lbl_pv = Label(master=self)
        self.lbl_degats = Label(master=self)
        self.lbl_energie = Label(master=self)
        self.lbl_niveau = Label(master=self)

        self.prb_pv = Progressbar(master=self, orient=HORIZONTAL, length=500)
        self.prb_degats = Progressbar(master=self, orient=HORIZONTAL, length=500)
        self.prb_energie = Progressbar(master=self, orient=HORIZONTAL, length=500)

        self.ind_sol = Label(master=self, image=self.images["sol_off"])
        self.ind_air = Label(master=self, image=self.images["air_off"])
        self.ind_pv = Label(master=self, text="pv", image=self.images["boost_off"], compound=LEFT)
        self.ind_energie = Label(master=self, text="nrg", image=self.images["boost_off"], compound=LEFT)
        self.ind_degat = Label(master=self, text="dgt", image=self.images["boost_off"], compound=LEFT)
        self.ind_vitesse = Label(master=self, text="vts", image=self.images["boost_off"], compound=LEFT)

        self.lbl_taille = Label(master=self)
        self.lbl_type = Label(master=self)
        self.lbl_statut = Label(master=self)
        self.lbl_name = Label(master=self)

    def configurer_widgets(self):
        """
        Configure les widgets déjà créés précédemment.
        """
        if self.unite_selectionnee is not None:
            self.img_unit.config(image=self.images["UNIT_" + str(self.unite_selectionnee.name)])
        else:
            self.img_unit.config(image=self.images["rien"])

        self.btn_attaque.config(image=self.images["attaque"],
                                 command=self.attaque)
        self.btn_repos.config(image=self.images["repos"],
                               command=self.repos)
        self.btn_deplacer.config(image=self.images["mouvement"],
                                  command=self.deplacer)

        if self.unite_selectionnee is not None:
            self.lbl_pv.config(text=str(self.unite_selectionnee.pv) + " / " + str(self.unite_selectionnee.max_pv))
            self.lbl_degats.config(
                text=str(self.unite_selectionnee.damage) + " / " + str(self.unite_selectionnee.max_damage))
            self.lbl_energie.config(
                text=str(self.unite_selectionnee.energy) + " / " + str(self.unite_selectionnee.max_energy))

            self.lbl_niveau.config(text=str(self.unite_selectionnee.level))

            self.prb_degats.config(maximum=self.unite_selectionnee.max_damage, value=self.unite_selectionnee.damage)
            self.prb_pv.config(maximum=self.unite_selectionnee.max_pv, value=self.unite_selectionnee.pv)
            self.prb_energie.config(maximum=self.unite_selectionnee.max_energy, value=self.unite_selectionnee.energy)

            if self.unite_selectionnee.attack_ground == 1:
                self.ind_sol.config(image=self.images["sol_on"])
            else:
                self.ind_sol.config(image=self.images["sol_off"])

            if self.unite_selectionnee.attack_sky == 1:
                self.ind_air.config(image=self.images["air_on"])
            else:
                self.ind_air.config(image=self.images["air_off"])

            if self.unite_selectionnee.energy == self.unite_selectionnee.max_energy:
                self.ind_energie.config(image=self.images["boost_on"])
            if self.unite_selectionnee.damage == self.unite_selectionnee.max_damage:
                self.ind_degat.config(image=self.images["boost_on"])
            if self.unite_selectionnee.pv == self.unite_selectionnee.max_pv:
                self.ind_pv.config(image=self.images["boost_on"])
            if self.unite_selectionnee.speed == self.unite_selectionnee.max_speed:
                self.ind_vitesse.config(image=self.images["boost_on"])

            if self.unite_selectionnee.energy > self.unite_selectionnee.max_energy:
                self.ind_energie.config(image=self.images["boost"])
            if self.unite_selectionnee.damage > self.unite_selectionnee.max_damage:
                self.ind_degat.config(image=self.images["boost"])
            if self.unite_selectionnee.pv > self.unite_selectionnee.max_pv:
                self.ind_pv.config(image=self.images["boost"])
            if self.unite_selectionnee.speed > self.unite_selectionnee.max_speed:
                self.ind_vitesse.config(image=self.images["boost"])

            if self.unite_selectionnee.pv < self.unite_selectionnee.max_pv:
                self.ind_pv = Label(master=self, text="pv", image=self.images["boost_off"], compound=LEFT)
            if self.unite_selectionnee.energy < self.unite_selectionnee.max_energy:
                self.ind_energie = Label(master=self, text="nrg", image=self.images["boost_off"], compound=LEFT)
            if self.unite_selectionnee.damage < self.unite_selectionnee.max_damage:
                self.ind_degat = Label(master=self, text="dgt", image=self.images["boost_off"], compound=LEFT)
            if self.unite_selectionnee.speed < self.unite_selectionnee.max_speed:
                self.ind_vitesse = Label(master=self, text="vts", image=self.images["boost_off"], compound=LEFT)

            self.lbl_name.config(text="type de troupe : " + self.unite_selectionnee.name)
            self.lbl_statut.config(text="statut de l'unité : " + self.unite_selectionnee.status)
            self.lbl_taille.config(text="taille de l'unité : " + str(self.unite_selectionnee.size))
            self.lbl_type.config(text="lieu de progression : " + str(self.unite_selectionnee.type))

        else:
            self.ind_pv.config(text="pv", image=self.images["boost_off"], compound=LEFT)
            self.ind_energie.config(text="nrg", image=self.images["boost_off"], compound=LEFT)
            self.ind_degat.config(text="dgt", image=self.images["boost_off"], compound=LEFT)
            self.ind_vitesse.config(text="vts", image=self.images["boost_off"], compound=LEFT)

            self.ind_sol.config(image=self.images["sol_off"])
            self.ind_air.config(image=self.images["air_off"])

            self.lbl_pv.config(text="0 / 0")
            self.lbl_degats.config(text="0 / 0")
            self.lbl_energie.config(text="0 / 0")

            self.lbl_niveau.config(text="0")

            self.prb_degats.config(maximum=500, value=0)
            self.prb_pv.config(maximum=500, value=0)
            self.prb_energie.config(maximum=500, value=0)

            self.lbl_name.config(text="type de troupe : ")
            self.lbl_statut.config(text="statut de l'unité : ")
            self.lbl_taille.config(text="taille de l'unité : ")
            self.lbl_type.config(text="lieu de progression : ")

    def charger_images(self):
        """
        Charge les différentes images nécessaires à l'interface.
        """
        data = charger_fichier_json("UNITS")
        self.images = {}
        tab = ["air_off", "air_on", "sol_on", "sol_off", "boost_on", "boost_off", "boost", "attaque", "repos",
               "mouvement", "rien"]
        for elt in tab:
            self.images[str(elt)] = PhotoImage(file="../pics/window/ICON_" + str(elt) + ".png")
        for unit in data:
            self.images[str(unit)] = PhotoImage(file="../pics/units/" + data[unit]["icon"])

    def placer_widgets(self):
        """
        Place les différents boutons et labels à l'initialisation.
        """
        self.img_unit.grid(column=0, row=0, columnspan=1, rowspan=7)

        self.btn_attaque.grid(column=3, row=7, columnspan=1, rowspan=2)
        self.btn_repos.grid(column=3, row=9, columnspan=1, rowspan=2)
        self.btn_deplacer.grid(column=4, row=7, columnspan=1, rowspan=2)

        self.lbl_pv.grid(column=2, row=1, columnspan=1, rowspan=1)
        self.lbl_degats.grid(column=2, row=3, columnspan=1, rowspan=1)
        self.lbl_energie.grid(column=2, row=5, columnspan=1, rowspan=1)
        self.lbl_niveau.grid(column=4, row=9, columnspan=1, rowspan=2)

        self.prb_pv.grid(column=1, row=0, columnspan=4, rowspan=1)
        self.prb_degats.grid(column=1, row=2, columnspan=4, rowspan=1)
        self.prb_energie.grid(column=1, row=4, columnspan=4, rowspan=1)

        self.ind_sol.grid(column=1, row=6, columnspan=1, rowspan=1)
        self.ind_air.grid(column=3, row=6, columnspan=2, rowspan=1)
        self.ind_pv.grid(column=0, row=7, columnspan=1, rowspan=1)
        self.ind_energie.grid(column=0, row=8, columnspan=1, rowspan=1)
        self.ind_degat.grid(column=0, row=9, columnspan=1, rowspan=1)
        self.ind_vitesse.grid(column=0, row=10, columnspan=1, rowspan=1)

        self.lbl_name.grid(column=1, row=7, columnspan=2, rowspan=1)
        self.lbl_taille.grid(column=1, row=8, columnspan=2, rowspan=1)
        self.lbl_type.grid(column=1, row=9, columnspan=2, rowspan=1)
        self.lbl_statut.grid(column=1, row=10, columnspan=2, rowspan=1)

    def actualiser(self, unit=None):
        """
        Met à jour l'interface avec l'unité sélectionnée.
        """
        self.unite_selectionnee = unit
        self.configurer_widgets()

    def repos(self):
        """
        Fonction appelée lors du clic sur le bouton "Repos".
        """
        print("Tu te reposes car tu es faible.")

    def attaque(self):
        """
        Fonction appelée lors du clic sur le bouton "Attaque".
        """
        print("Et non, tu n'as pas assez d'argent pour acheter un obus.")

    def deplacer(self):
        """
        Fonction appelée lors du clic sur le bouton "Déplacer".
        """
        print("Tu fuis, espèce de lâche.")


if __name__ == "__main__":
    unite_1 = Unit("UNIT_chasseur")
    unite_2 = Unit("UNIT_bombardier")
    unite_3 = Unit("UNIT_tank")

    unite_2.mut_boost_pv(2)
    unite_2.ajoute_energie(-150)
    unite_2.mut_attack_sky(1)

    fenetre = Tk()
    ecran = Interface_Unite(fenetre)
    btn1 = Button(fenetre,
                  text="Unité 1",
                  command=lambda: ecran.actualiser(unite_1))
    btn2 = Button(fenetre,
                  text="Unité 2",
                  command=lambda: ecran.actualiser(unite_2))
    btn3 = Button(fenetre,
                  text="Désélectionner",
                  command=lambda: ecran.actualiser(unite_3))
    btn4 = Button(fenetre,
                  text="Désélectionner",
                  command=lambda: ecran.actualiser(None))
    ecran.grid(row=0, column=0, columnspan=4)
    btn1.grid(row=1, column=0)
    btn2.grid(row=1, column=1)
    btn3.grid(row=1, column=2)
    btn4.grid(row=1, column=3)
    fenetre.mainloop()
