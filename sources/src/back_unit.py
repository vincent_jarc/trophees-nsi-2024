from outils import *

class Unit:

    def __init__(self, unit_id:str):
        data = charger_fichier_json("UNITS")
        self.id = unit_id
        self.tag = None
        self.name = data[unit_id]["name"]
        self.damage = data[unit_id]["damage"]
        self.max_damage = data[unit_id]["damage"]
        self.pv = data[unit_id]["pv"]
        self.max_pv = data[unit_id]["pv"]
        self.speed = data[unit_id]["speed"]
        self.max_speed = data[unit_id]["speed"]
        self.rest_time = data[unit_id]["rest_time"]
        self.energy = data[unit_id]["energy"]
        self.max_energy = data[unit_id]["energy"]
        self.cost_gold = data[unit_id]["cost-gold"]
        self.cost_population = data[unit_id]["cost-population"]
        self.cost_food = data[unit_id]["cost-food"]
        self.construction_time = data[unit_id]["construction_time"]
        self.icon = data[unit_id]["icon"]
        self.level = data[unit_id]["level"]
        self.size = data[unit_id]["size"]
        self.type = data[unit_id]["type"]["milieu"]
        self.attack_ground = data[unit_id]["type"]["attack_ground"]
        self.attack_sky = data[unit_id]["type"]["attack_sky"]
        self.sprite = data[unit_id]["sprite"]
        self.status = "ACTIF"  # Valeurs possibles : ACTIF, SOIN, ENDORMI, FORTIFIE


    #################################################################################
    #################################################################################
    #################################################################################

    def __str__(self)->str:
        """
        la fonction d'affichage (__str__) permet d'afficher semblable au descriptif de l'unité en jeu
        """
        # choix = int(input("1 ou 2 ?????? :"))
        # if choix == 1:
        text = ""
        text+= self.name + ": \n"
        text+= "déguat : " + str(self.damage) +  "/" + str(self.max_damage) + "\n"
        text+= "points de vie : " + str(self.pv) + "/" + str(self.max_pv) + "\n"
        text+= "vitesse : " + str(self.speed) + "/" + str(self.max_speed) + "\n"
        text+= "energie : " + str(self.energy) + "/" + str(self.max_energy) + "\n"
        text+= "niveau : " + str(self.level) + "\n"
        text+= "taille : "+str(self.size) + "\n"
        text+= "environement de combat : " + str(self.type)
        '''else:
            text+= "achat d'un "
            text+= str(self.name)
            text+= " de niveau : "
            text+= str(self.level)
            text+= " : \n"
            text+= "cout : \n\t or : "
            text+= str(self.cost_gold)
            text+= "\n\t population : "
            text+= str(self.cost_population)
            text+= "\n\t nourriture : "
            text+= str(self.cost_food)
            text+= "\n"
            text+= "temps de fabrication : \t"
            text+= str(self.construction_time)
            text+= " unité de temps"'''
        return text

    def __repr__(self):
        """
        la fonction d'affichage (__repr__) permet d'afficher le type et le niveau de l'unité
        """
        text= str(self.name)+" de niveau : "+str(self.pv)
        return text

    ####################################################################################
    ####################################################################################
    ####################################################################################

    def est_mort(self):
        """
        la fonction (est_mort) effectue un teste afin de savoir le statue de l'unité (en vie, mort)
        """
        if self.pv <= 0:
            return True
        else:
            return False



    def est_blesse(self):
        """
        la fonction (est blesse) effectue un test permettant de savoir si l'unité possede toutes ses vies.
            son utilité est pour l'instant inconnu.
        """
        if 0 < self.pv < self.max_pv:
            return True
        else:
            return False

    def est_en_soin(self) -> bool:
        """Indique si l'unité est en train de se soigner"""
        return self.status == "SOIN"

    def est_endormi(self) -> bool:
        """Indique si l'unité a passé son tour"""
        return self.status == "ENDORMI"

    def est_fortifie(self) -> bool:
        """Indique si l'unité est fortifiée"""
        return self.status == "FORTIFIE"


    def attack_ground(self):
        """
        la fonction (attack_ground) permet de savoir si une unité est en capacité a attaquer une unité terrestre (de type : sol/ground)
        """
        if self.type["attack_ground"] == 1:
            return True
        else:
            return False



    def attack_sky(self):
        """
        la fonction (attack_sky) permet de savoir si une unité est en capacité a attaquer une unité aerienne (de type : air/sky)
        """
        if self.type["attack_sky"] == 1:
            return True
        else:
            return False

    """ A DEPLACER
    def can_buy(self, gold:int, pop:int, food:int):
        if self.cost_gold > gold:
            return False
        if self.cost_population > pop:
            return False
        if self.cost_food>food:
            return False
        else:
            return True"""

    def peut_bouger(self, energy:int=1):
        """
        La fonction (peut_bouger) permet de savoir si une unité a de l'énergie et ainsi savoir
        s'il est envisageable qu'elle se déplace
        """
        if self.energy < energy:
            return False
        else:
            return True

    def peut_agir(self) -> bool:
        if self.est_endormi() or self.est_en_soin() or self.est_fortifie():
            return False
        else:
            return self.energy > 0

    def can_fly(self):
        """
        la fonctrion (can_fly) permet de savoir si une unité est aérienne et donc par conclusion terrestre si le resultat est "False"
        """
        if self.type["milieu"] == "sky":
            return True
        else:
            return False



    def calcule_degats(self, other):
        """
        la fonction (calcule_degats) permet de calculer les dégâts d'une attaque en fonction des points de dégâts et du
        différentiel de niveau entre les unités
        """
        damage=(self.damage*self.level) // other.level
        if self.damage//2 > damage:
            return self.damage//2
        elif self.damage + self.damage//2 < damage:
            return self.damage+self.damage//2
        else:
            return damage



    def calcule_deplacement(self):
        """
        la fonction (calcul_déplacement) permet de calculer le nombre d'énergie necessiare à un deplacement sur 1unité de distance en fonction de
        son poid(size) et de son taux de blessure (plus l'uité est blessée plus est a besoin d'ener'gie pour se déplacer) il est donc possible grace à un
        boost de pv de consomé moins d'énegie .
        """
        cout = self.size//((self.max_pv-self.pv)//self.max_pv)
        if cout > self.max_energy:
            return self.max_energy
        return cout

    ###############
    #  MUTATEURS  #
    ###############
    """
    14 mutateur qui permettent pour l'instant d'effectuer les operations de bases.
    il n'y a pas ici de recouvrement de pv échelonné.
    les boost utilisent des coefficient multiplicateur et ne sont pas soumis à un plafond.
    """
    ######################
    # MUTATEURS NÉGATIFS #
    ######################

    def enleve_energie(self, value:int):
        """
        la fonction (enleve_energie) permet de baisser l'énergie' de l'unité d'une valeur x
        """
        if self.energy-value <= 0:
            self.energy=0
        else:
            self.energy -= value

    def enleve_vitesse(self, value:int):
        """
        la fonction (enleve_vitesse) permet de baisser lvitesse de l'unité d'une valeur x
        """
        if self.speed - value < 0:
            self.speed = 0
        else:
            self.speed -= value


    def enleve_pv(self, value:int):
        """
        la fonction (enleve_pv) permet de baisser les pv de l'unité d'une valeur x
        """
        if self.damage - value < 0:
            self.damage = 0
        else:
            self.damage -= value

    ############################################################################
            # MUTATEUR REINITIALISATION #

    def ajoute_pv(self, value:int):
        """
        la fonction (ajoute_pv) permet d'augmenter les pv de l'unité d'une valeur x
        """
        if self.pv + value >= self.max_pv:
            self.pv = self.max_pv
        else:
            self.pv += value

    def ajoute_energie(self, value:int):
        """
        La fonction (ajoute_energie) permet l'augmentation de la variable energie d'une valeur x
        """
        if self.energy + value > self.max_energy:
            self.energy = self.max_energy
        else:
            self.energy += value

    def recouvre_vitesse(self):
        """
        la fonction (recouvre_vitesse) permet de réinitialiser la variable self.speed
        """
        self.speed = self.max_speed

    def recouvre_domage(self):
        """
        la fonction (recouvre_domage) permet de réinitialiser la variable self.domage
        """
        self.damage = self.max_damage

    ##################
    # MUTATEUR BOOST #
    ##################

    def mut_boost_pv(self,x:int):
        """
        Les boost permettent une augmentation des variables sans plafond et depende d'un coeff
        """
        assert(x>0)
        self.pv = self.pv*x

    def mut_boost_energy(self, x: int):
        """
        Les boost permettent une augmentation des variables sans plafond et depende d'un coeff
        """
        assert(x > 0)
        self.energy = self.energy*x

    def mut_boost_speed(self, x: int):
        """
        Les boost permettent une augmentation des variables sans plafond et depende d'un coeff
        """
        assert(x>0)
        self.speed = self.speed*x

    def mut_boost_damage(self, x: int):
        """
        les boost permettent une augmentation des variables sans plafond et depende d'un coeff
        """
        assert(x > 0)
        self.damage = self.damage*x

    ####################
    # MUTATEUR BINAIRE #
    ####################

    def mut_attack_ground(self, x:int):
        """
        Les mutateurs binaires permettent de modifier les cible q'une unité peut atteindre pour l'handicaper
        ou lui conferer un avantage.
        """
        assert(x==1 or x==0)
        self.attack_ground = x

    def mut_attack_sky(self,x:int):
        """
        les mutateur binaires permettent de modifier les cible q'une unité peut atteindre pour l'handicaper ou lui conferer un avantage.
        """
        assert(x==1 or x==0)
        self.attack_sky = x

    ###########################################################################
            # DIVERS #



    def obtient_etiquette(self, tag:str):
        """
        la fonction (obtient_etiquette) permet d'atribuer un tag a une unité
        """
        self.tag = tag


    def s_endort(self):
        """
        la fonction(s_endors) permet de convertir l'eneregie restante à une unité en pv. Cepandant tous mouvement est donc rendu impossible jusqu'a la fin du tour
        """
        self.status = "ENDORMI"
        self.ajoute_pv(self.energy)
        self.energy = 0

    def se_reveille(self):
        self.status = "ACTIF"

    def se_fortifie(self):
        """Place l'unité en statut FORTIFIE"""
        self.status = "FORTIFIE"

    def se_soigne(self):
        """Place l'unité en statut ENDORMI, lorsqu'elle passe son tour"""
        self.status = "ENDORMI"

    def annuler_boost(self):
        """Rétablit les caractéristiques d'une unité"""
        if self.pv > self.max_pv:
            self.pv = self.max_pv
        if self.damage > self.max_damage:
            self.damage = self.max_damage
        if self.speed > self.max_speed:
            self.speed = self.max_speed
        if self.energy > self.max_energy:
            self.energy = self.max_energy

    def commence_tour(self):
        """
        La fonction (commence_tour) permet de reinitialiser les variables qui aurait pus etre soumises à des boost
        """
        self.annuler_boost()
        # TODO : RÉTABLIR LES POINTS DE MOUVEMENTS !!!!
        if self.est_endormi():
            self.se_reveille()
        if self.est_en_soin():
            self.ajoute_pv(1)  # TODO : indiquer la valeur par défaut du soin !!!
            if not self.est_blesse():
                self.se_reveille()



    def attaque_en_duel(self, other):
        """
        La fonction (attaque_en_duel) permet à deux unités de s'attaquer simultanément.
        """
        degats1 = self.calcule_degats(other)
        degats2 = other.calcule_degats(self)
        other.enleve_pv(degats1)
        self.enleve_pv(degats2)

    def attaque_a_distance(self, other):
        """
        La fonction (attaque_a_distance) permet à une unité d'en s'attaquer une autre.
        """
        degats = self.calcule_degats(other)
        other.enleve_pv(degats)

    def peut_attaquer(self, other) -> bool:
        """TODO : Renvoie True ou False, selo que self peut attaquer other ou pas"""
        return True

    def positioner(self, coordonnees: tuple):
        """
        la fonction (positioner) permet de réatribuer des coordonées (x,y) à l'unité
        """
        self.x = coordonnees[0]
        self.y = coordonnees[1]


###########################################################
#    EXEMPLE d'utilisation

if __name__ == "__main__":
    Leclerc = Unit("UNIT_tank")
    VBC = Unit("UNIT_transport")
    # f_35.acces_damage()
    # f_35.mut_lost_damage(50)
    # f_35.acces_speed()
    # f_35.mut_boost_speed(1.5)
    # f_35.acces_pv()
    # f_35.mut_take_damage(500)
    # f_35.acces_energy()
    # f_35.acces_lost_pv()
    while not Leclerc.est_mort() and not VBC.est_mort():
        Leclerc.attaque_en_duel(VBC)
    print("victory")
    if Leclerc.est_mort():
        print(VBC)
        print([Leclerc])
    else:
        print(Leclerc)
        print([VBC])
