# CrÃ©Ã© par Maxence, le 18/10/2023 en Python 3.7
from outils import charger_fichier_json


class Building:
    def __init__(self, building_id: str, datas: dict):

        self.name = datas[building_id]["name"]
        self.id = building_id
        self.price = datas[building_id]["stage"]["1"]["price"]
        self.max_time = datas[building_id]["stage"]["1"]["building_time"]
        self.current_time = self.max_time
        self.built = False
        self.pop_created = datas[building_id]["stage"]["1"]["pop_created"]
        self.gold_created = datas[building_id]["stage"]["1"]["gold_created"]
        self.food_created = datas[building_id]["stage"]["1"]["food_created"]
        self.image_url = "../pics/buildings/" + datas[building_id]["stage"]["image_url"]
        # TODO : à clarifier
        self.military_units = datas[building_id]["stage"]["1"]["military_units"]
        self.civil_units = datas[building_id]["stage"]["1"]["civil_units"]
        # TODO : utile ?
        # self.rest_acceleration_percentage = datas[building_id]["stage"]["1"]["rest_acceleration_percentage"]
        # self.status_zone = datas[building_id]["stage"]["1"]["status_zone"]
        # self.building_size = datas[building_id]["stage"]["building_size"]

    def __str__(self) -> str:
        """renvoie toutes les informations sur le batiment"""
        txt = "\t {}\n".format(self.name)
        txt += "COUT  : {}\n".format(self.price)
        txt += "TEMPS : {0}/{1}\n".format(self.current_time, self.max_time)
        txt += "POPULATION : " + str(self.pop_created)+"\n"
        txt += "OR : " + str(self.gold_created)+"\n"
        txt += "NOURRITURE : " + str(self.food_created)+"\n"
        return txt

    def __repr__(self):
        """renvoie le minimum d'info"""
        return "{0} ({1}/{2})".format(self.name, self.current_time, self.max_time)

    def est_acheve(self) -> bool:
        """Indique si le batiment a fini d'être construit"""
        return self.built

    def est_endommage(self) -> bool:
        """endommage le batiment en remettant en construction"""
        if self.built and self.current_time > 0:
            return True
        else:
            return False

    def recolte(self) -> dict:
        """Récolte les ressources produites et les stocke dans un dico"""
        ressources = {"food": self.food_created,
                      "gold": self.gold_created
                      }
        if self.est_endommage() or not self.est_acheve():
            return {"food": 0, "gold": 0}
        else:
            return ressources

    def unites_entrainables(self)->list:
        """"""
        return self.military_units + self.civil_units

    def peut_entrainer(self, unit_id:str="ANY") -> bool:
        """Indique si l'identifiant unit_id fait partie des identifiants
        d'unités entraînables"""
        if unit_id == "ANY":
            return self.unites_entrainables() != []
        else:
            return unit_id in self.unites_entrainables()

    def devient_endommage(self):
        """Si le bâtiment est endommagé, le temps de construction est relevé"""
        # TODO : formule à préciser
        self.current_time = self.max_time // 2

    def devient_acheve(self):
        """Achève la construction"""
        self.built = True


    def construit(self, value:int=1):
        """reduit le temps de construction du batiment"""
        self.current_time -= value
        if self.current_time < 0:
            self.current_time = 0
        if self.current_time == 0:
            self.devient_acheve()

    def ajoute_or(self, value:int):
        """ajoute de l'or au joueur"""
        self.gold_created += value

    def enleve_or(self, value:int):
        """enleve de l'or au joueur"""
        self.gold_created -= value
        if self.gold_created < 0:
            self.gold_created = 0

    def ajoute_nourriture(self, value:int):
        """ajoute de l'or au joueur"""
        self.food_created += value

    def enleve_nourriture(self, value:int):
        """enleve de l'or au joueur"""
        self.food_created -= value
        if self.food_created < 0:
            self.food_created = 0

    def ajoute_population(self, value:int):
        """ajoute de l'or au joueur"""
        self.pop_created += value

    def enleve_population(self, value:int):
        """enleve de l'or au joueur"""
        self.pop_created -= value
        if self.pop_created < 0:
            self.pop_created = 0




if __name__ == '__main__':
    datas = charger_fichier_json("BUILDINGS")
    aeroport = Building("BUILD_aeroport", datas)
    print(aeroport)
    aeroport.construit(25)
    print("---------")
    print("Construction !!!")
    print(aeroport)
    print("Récolte", aeroport.recolte())
    print("---------")
    print("Dégâts !!!")
    aeroport.devient_endommage()
    print(aeroport)
    print("Récolte", aeroport.recolte())
    print("Bombardier entrainable ?", aeroport.peut_entrainer("UNIT_bomber"))
    print("Infanterie entrainable ?", aeroport.peut_entrainer("UNIT_soldier"))
