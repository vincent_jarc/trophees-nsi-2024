from tkinter import *
from back_city import City
from outils import *
from back_building import Building

class Interface_Construction(Toplevel):
    """Classe qui gère la construction des batiments """
    def __init__(self, master, city: City):
        Toplevel.__init__(self, master)
        self.master = master
        self.ville_selectionnee = city
        self.building_selectionnee = self.ville_selectionnee.in_progress
        self.charger_images()
        self.creer_widgets()
        self.placer_widgets()
        self.configurer_widgets()

    def charger_images(self):
        """Charge les images des différents batiments"""
        self.images = {}
        self.images[None] = PhotoImage(file="../pics/window/ICON_pas_travail.png")
        self.images["BTN_OK"] = PhotoImage(file="../pics/window/ICON_confirmer.png")
        for building in self.ville_selectionnee.batiments_acheves():
            self.images[building.id] = PhotoImage(file=building.image_url)
        for building in self.ville_selectionnee.batiments_non_construits():
            self.images[building.id] = PhotoImage(file=building.image_url)

    def creer_widgets(self):
        """Créer et donne l'utilité des différents boutons"""
        self.lbl_cityname = Label(master=self, text=self.ville_selectionnee.name)
        self.btn_quitter = Button(master=self, image=self.images["BTN_OK"], command=self.destroy) # text="Quitter",
        self.zone_1 = Frame(master=self)
        self.zone_2 = Frame(master=self)
        self.lbl_en_construction = Label(master=self,
                                         text="En construction :\t",
                                         image=self.images[None],
                                         compound=BOTTOM)
        self.lbl_en_construction_info = Label(master=self,
                                         text="Aucun bâtiment sélectionné")
        self.lbl_batiments_construits = []
        self.btn_non_construit = []
        for building in self.ville_selectionnee.batiments_acheves():
            self.lbl_batiments_construits.append(Label(master=self.zone_2,
                                                       image=self.images[building.id]))
        for building in self.ville_selectionnee.batiments_non_construits():
            self.btn_non_construit.append(Button(master=self.zone_1,
                                                 image=self.images[building.id] ,
                                                 command=lambda x=building:self.on_btn_batiment_click(x)))

    def configurer_widgets(self):
        """Rend le code plus joli façon renault megane rs3 double injecteur sans pot
        d'échappement avec compresseur à air et v12 integré"""
        self.lbl_cityname.config(font="Arial 30 bold")
        self.lbl_en_construction.config(font="Arial 12 italic")
        self.lbl_en_construction_info.config(font="Arial 14", justify=LEFT)
        if self.building_selectionnee is None:
            self.lbl_en_construction_info.config(fg="red")
        else:
            self.lbl_en_construction_info.config(fg="black")

    def placer_widgets(self):
        """place les widgets sur la fenetre"""
        self.lbl_cityname.grid(row=0, column=0, columnspan=3, rowspan=1)
        self.zone_1.grid(row=1, column=0, rowspan=3, sticky=N)
        for i, btn in enumerate(self.btn_non_construit):
            btn.grid(column=i%3, row=i//3)

        self.lbl_en_construction.grid(row=1, column=1, sticky=N)
        self.lbl_en_construction_info.grid(row=2, column=1, sticky=N)
        self.btn_quitter.grid(row=3, column=1, sticky=S)

        self.zone_2.grid(row=1, column=2, rowspan=3, sticky=N)
        for i, lbl in enumerate(self.lbl_batiments_construits):
            lbl.grid(column=i%3, row=i//3)


    def actualiser(self):
        """Met à jour les informations en fonction du batiment selectionné"""
        self.building_selectionnee = self.ville_selectionnee.in_progress
        if self.building_selectionnee is None:
            self.lbl_en_construction.config(image= self.images[None])
            self.lbl_en_construction_info.config(text="Aucun bâtiment sélectionné",
                                                 fg="red")
        else:
            self.lbl_en_construction.config(image= self.images[self.building_selectionnee.id])
            self.lbl_en_construction_info.config(text=str(self.building_selectionnee),
                                                 fg="black")

    def on_btn_batiment_click(self, building: Building):
        """Met le batiment selectionné en construction"""
        self.ville_selectionnee.met_en_chantier(building.id)
        self.building_selectionnee = self.ville_selectionnee.in_progress
        self.actualiser()



if __name__ == "__main__":
    datas = charger_fichier_json("BUILDINGS")
    ville_1 = City("CITY_Washington_D_C", charger_carte("MAP_WORLD.json"))

    ville_1.met_en_chantier("BUILD_piste")
    ville_1.construit(1000)
    ville_1.met_en_chantier("BUILD_mine")
    ville_1.construit(1000)


    fenetre = Tk()
    btn = Button(fenetre,
                 text="Construire",
                 command=lambda: Interface_Construction(fenetre, ville_1))
    btn.grid()
    fenetre.mainloop()