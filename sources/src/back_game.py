from back_city import City
from back_unit import Unit
from back_players import Player
from outils import *

class Jeu:
    def __init__(self, map_adress:str, first_player_id:str):
        self.map_datas = charger_carte(map_adress)
        self.first_player_id = first_player_id
        self.villes = dict()
        self.joueurs = []
        self.tour = 1
        self.joueur_actif = None
        self.unite_selectionnee = None
        self.ville_selectionnee = None
        self.setup_game()

    def __str__(self) -> str:
        txt = "JOUEURS :\t\t" + str(self.joueurs)
        txt += "\nDIMENSIONS :\t{0} x {1}".format(self.map_datas["largeur"], self.map_datas["hauteur"])
        txt += "\nTOUR :\t\t\t{0} ({1})".format(self.tour, self.joueur_actif.name)
        return txt

    def setup_game(self):
        self.creer_villes()
        self.connecter_villes()
        self.creer_joueurs()
        self.attribuer_villes()

    def creer_villes(self):
        for city_id in self.map_datas['cities']:
            self.villes[city_id] = City(city_id, self.map_datas)

    def connecter_villes(self):
        for villeD_id in self.map_datas["link"]:
            villeD = self.villes[villeD_id]
            for villeA_id in self.map_datas["link"][villeD_id]:
                villeA = self.villes[villeA_id]
                villeD.se_connecte_a(villeA)

    def attribuer_villes(self):
        for ville_id in self.map_datas["cities"]:
            owner_id = self.map_datas["cities"][ville_id]["owner"]
            ville = self.villes[ville_id]
            proprio = self.joueurs[self.numero_joueur(owner_id)]
            ville.change_proprietaire(owner_id)
            proprio.ajoute_ville(ville)

    def creer_joueurs(self):
        self.joueurs.append(Player(self.first_player_id))
        for player_id in self.map_datas["playable_nation"]:
            if player_id != self.first_player_id:
                self.joueurs.append(Player(player_id))
        self.joueurs.append(Player("NATION_Neutral"))
        self.joueur_actif = self.joueurs[0]

    def tour_actuel(self):
        return self.tour
    def joueur_actuel(self):
        return self.joueurs.index(self.joueur_actif)

    def numero_joueur(self, player_id:str)->int:
        i = 0
        for player in self.joueurs:
            if player.id == player_id:
                return i
            i+=1
        return None
    def id_joueur(self, player_num:str) -> int:
        return self.joueurs[player_num].id

    def a_fini_de_jouer(self) -> bool:
        return self.joueur_actif.a_fini_tour()

    def est_mort(self, num:int) -> bool:
        return self.joueurs[num].est_mort()

    def chercher_unite(self, tag:str) -> Unit:
        for ville in self.villes:
            for unit in ville.units:
                if unit.tag == tag:
                    return unit


    def joueur_suivant(self):
        current_player_index = self.joueur_actuel()
        next_player_index = (current_player_index + 1) % len(self.joueurs)
        if next_player_index == 0:
            self.tour += 1
        self.joueur_actif = self.joueurs[next_player_index]

    def creer_unite(self, unit_id):
        unit = Unit(unit_id)
        self.joueur_actif.ajoute_unite(unit)
        self.ville_selectionnee.ajoute_unite(unit)

    def selectionner_unite(self, unit):
        self.unite_selectionnee = unit

    def ville_depart(self, unit) -> City:
        for city_id in self.villes:
            city = self.villes[city_id]
            if unit in city.units:
                return city

    def deplacer_unite(self, city_id):
        destination = self.chercher_ville(city_id)
        if self.unite_selectionnee and destination:
            if self.peut_deplacer_unite(self.unite_selectionnee, destination):
                print(f"l'unite se déplace à {destination.name}")
                depart = self.ville_depart(self.unite_selectionnee)
                depart.enleve_unite(self.unite_selectionnee)
                destination.ajoute_unite(self.unite_selectionnee)
            else:
                print("l'unite ne peut pas se déplacer ici")

    def attaque_en_duel(self, city_id):
        destination = self.chercher_ville(city_id)
        if self.unite_selectionnee and destination:
            print(f"duel avec {destination.name}")
            self.unite_selectionnee.attaque_en_duel(destination.units[0])

    def attaque_a_distance(self, city_id):
        destination = self.chercher_ville(city_id)
        if self.unite_selectionnee and destination:
            print(f"attaque de {destination.name}")
            self.unite_selectionnee.attaque_a_distance(destination.units[0])

    def chercher_ville(self, city_id):
        for id_ville in self.villes:
            if id_ville == city_id:
                return self.villes[id_ville]
        return None

    def selectionner_ville(self, city_id):
        self.ville_selectionnee = self.chercher_ville(city_id)

    def peut_deplacer_unite(self, unit:Unit, destination:City):
        depart = self.ville_depart(unit)
        if destination not in depart.neighbors:
            print(destination.name, "est trop éloignée")
            return False
        if destination.owner_id != depart.owner_id:
            print(destination.name, "ne vous appartient pas")
            return False
        return True

if __name__ == "__main__":
    adresse_carte = "../maps/MAP_WORLD.json"
    first_player_id = "NATION_France"
    jeu = Jeu(adresse_carte, first_player_id)
    print(jeu)
    jeu.joueur_suivant()
    print(jeu)
    print(jeu.villes["CITY_Paris"])
    print(jeu.villes["CITY_Paris"].owner_id)

    jeu.selectionner_ville("CITY_Paris")
    jeu.creer_unite("UNIT_infanterie")
    jeu.selectionner_unite(jeu.villes["CITY_Paris"].units[0])

    print(jeu.villes["CITY_Paris"])
    print(jeu.unite_selectionnee)
    jeu.deplacer_unite("CITY_Moskow")

    jeu.villes["CITY_Moskow"].change_proprietaire("NATION_France")
    jeu.deplacer_unite("CITY_Moskow")

    jeu = Jeu("../maps/MAP_ASIA.json", "NATION_Japan")
    print(jeu)





