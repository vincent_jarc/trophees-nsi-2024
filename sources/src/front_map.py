import tkinter
from outils import *
from back_game import Jeu
from back_unit import *


class Interface_Carte(tkinter.Frame):
    def __init__(self, master, game:Jeu):
        tkinter.Frame.__init__(self, master)
        self.master = master
        self.game = game
        self.largeur = self.game.map_datas["largeur"]
        self.hauteur = self.game.map_datas["hauteur"]
        self.background_adress = self.game.map_datas["image"]

        self.fond = tkinter.Canvas(self,
                                   width=self.largeur,
                                   height=self.hauteur)
        self.fond.grid()
        self.images = dict()
        self.charger_images()
        self.generation()

    def charger_images(self):
        self.images["background"] = tkinter.PhotoImage(file=self.background_adress)
        unit_datas = charger_fichier_json("UNITS")
        path = "../pics/units/"
        for unit_id in unit_datas:
            self.images[unit_id] = tkinter.PhotoImage(file=path+unit_datas[unit_id]["sprite"])


    def generation(self):
        self.fond.create_image(0, 0, anchor=tkinter.NW,
                               image=self.images["background"])
        for city in self.game.villes.values():
            x1, y1 = city.localisation
            color = self.game.joueurs[self.game.numero_joueur(city.owner_id)].color
            tag1 = self.fond.create_oval(x1 - 5, y1 - 5, x1 + 5, y1 + 5,
                                         activeoutline=color,
                                         activefill="white",
                                         fill=color,
                                         width=2)
            tag2 = self.fond.create_text(x1 - len(city.name) // 2, y1 - 15,
                                         activefill=color,
                                         font="Purisa 14 bold",
                                         text=city.name)
            self.fond.tag_bind(tag1,
                               "<Button-1>",
                               lambda evt, c=city.id: self.on_city_click(c))
            self.fond.tag_bind(tag2,
                               "<Button-1>",
                               lambda evt, c=city.id: self.on_city_click(c))
            for neighbor in city.neighbors:
                x2, y2 = neighbor.localisation
                self.fond.create_line(x1, y1, x2, y2)
    
    def dessiner_unite(self, unit:Unit):
        id, x, y = unit.id, unit.x, unit.y
        if unit.tag is None:
            unit.tag = self.fond.create_image(x, y, image=self.images[id])
            self.fond.tag_bind(unit.tag,
                               "<Button-1>",
                               lambda evt: self.on_unit_click(unit))
        print(unit.tag)

    def effacer_unite(self, unit:Unit):
        self.fond.delete(unit.tag)
        unit.tag = None

    def deplacer_unite(self, unit:Unit, destination:tuple):
        unit.positioner(destination)
        self.fond.coords(unit.tag, unit.x, unit.y)

    def on_city_click(self, city_id: str):
        self.game.selectionner_ville(city_id)
        print(self.game.ville_selectionnee)
        self.master.event_generate("<<INFO-UPDATE>>")

    def on_unit_click(self, unit: Unit):
        self.game.selectionner_unite(unit)
        print(self.game.unite_selectionnee)
        self.master.event_generate("<<INFO-UPDATE>>")


if __name__ == "__main__":
    game = Jeu("../maps/MAP_WORLD.json", "NATION_FRANCE")
    u = Unit("UNIT_tank")
    u.positioner((200, 300))

    fenetre = tkinter.Tk()
    ecran = Interface_Carte(fenetre, game)
    btn1 = tkinter.Button(fenetre,
                          text="Créer unité",
                          command=lambda: ecran.dessiner_unite(u))
    btn2 = tkinter.Button(fenetre,
                          text="Effacer unité",
                          command=lambda: ecran.effacer_unite(u))
    btn3 = tkinter.Button(fenetre,
                          text="Déplacer unité",
                          command=lambda: ecran.deplacer_unite(u, (800, 400)))
    ecran.grid(row=0, column=0, columnspan=3)
    btn1.grid(row=1, column=0)
    btn2.grid(row=1, column=1)
    btn3.grid(row=1, column=2)
    fenetre.mainloop()










