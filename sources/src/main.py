from tkinter import Tk
from front_screens import *


class Application(Tk):
    def __init__(self, fullscreen:bool = True):
        Tk.__init__(self)
        self.ecran = Ecran_Accueil(self)
        self.ecran.grid(row=0, column=0)
        if fullscreen:
            self.attributes("-fullscreen", True)
        self.configure(bg='black')
        self.title('groupe-A')
        # self.iconbitmap("../pics/window/ICON_icone.ico")


if __name__ == "__main__":
    fenetre = Application(fullscreen=False)
    fenetre.mainloop()


