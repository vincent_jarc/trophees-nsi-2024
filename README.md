[[_TOC_]]

# Description

Groupe-A est un jeu de type 4X (eXplore, eXpand, eXploit and eXterminate ) qui doit être basé sur les notions vues en cours comme les classes et les graphes. Notre groupe a choisi le  thème de la guerre froide. Ainsi, les objectifs sont de réaliser un jeu opérationnel et un maximum en accord avec les fait historiques bien que l’issue du jeu dépende entièrement des joueurs. Ce sera donc un jeu de stratégie qui pourra se jouer à plusieurs et qui aura pour but de battre ses adversaires.

[Video de présentation Peertube](https://tube-sciences-technologies.apps.education.fr/w/cmDAQDZYDGHmqGhPwZZYXv)

![image](https://gitlab.com/vincent_jarc/trophees-nsi-2024/-/raw/main/doc/screen.png?ref_type=heads)

***

# Membres du projet

- Tabuteau Maxence
- Maksymenko Kyrylo
- Mellier Valentin

***

# Installation

Télécharger le projet et exécutez le fichier `main.py` situé dans le dossier `sources/src`. 

***

# Dépendances

Le jeu utilise Python en version 3.6 ou supérieur ainsi que les bibliothèques suivantes : 

- `json`
- `tkinter`
- `vlc`